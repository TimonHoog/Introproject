import mysql.connector
from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)
#conneting with the database
db = mysql.connector.connect(host="localhost", user="Timon", passwd="Hoogendoornmysql23",database="Accomodo") 

#main page were users can input there house details and can view available rooms
@app.route('/')
def main_page():
    mycursor = db.cursor()
    #select all the details from every house in the database
    mycursor.execute("SELECT * FROM house")

    myresult = mycursor.fetchall()
    #render the html page
    return render_template ('kamer-upload-pagina.html', myresult= myresult )
    
    
# page with the results of filling in the form
@app.route('/result', methods = ['POST'])
def result():
    #retrieve data from the form
    adress= request.form['adress']
    postcode= request.form['postcode']
    housenumber = request.form ['housenumber']
    oppervlak=request.form ['oppervlak']
    price= request.form['price']
    beschrijving= request.form['beschrijving']
    roomdetails=request.form['roomdetails']
    cursor = db.cursor()
    #insert the data from the form into the database
    sql = "INSERT INTO house (adress, postcode, housenumber, oppervlak, price, beschrijving, roomdetails) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    val = (adress, postcode, housenumber, oppervlak, price, beschrijving, roomdetails)
    cursor.execute(sql,val)
    db.commit()
    #render results page
    return render_template('result.html', adress = adress, postcode = postcode, housenumber=housenumber, oppervlak=oppervlak, price=price, beschrijving=beschrijving, roomdetails=roomdetails)


#some setup for the django 
if __name__ == "__main__":
    app.config['TEMPLATES_AUTO_RELOAD']=True
    app.config['DEBUG'] = True
    app.config['SERVER_NAME'] = "127.0.0.1:5000"
    app.run()