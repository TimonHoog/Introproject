import mysql.connector
#maak eerst database aan
mydb = mysql.connector.connect(
  host="localhost",
  user="", #vul user in
  password="", # vul password in
  database="" # geef naam database
)

mycursor = mydb.cursor()

#maak eerst database aan voordat je deze toevoegd

mycursor.execute("CREATE TABLE House (houseid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, adress VARCHAR(255), postcode VARCHAR(255), huisnummer VARCHAR(255), oppervlak FLOAT, beschrijving VARCHAR(1000), price DECIMAL(10,2))")

mycursor.execute("CREATE TABLE Users (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR(255), lastname VARCHAR(255), email VARCHAR(255), phone INTEGER, homeowner BOOLEAN, house INTEGER, FOREIGN KEY (house) REFERENCES House (houseid) ON UPDATE CASCADE ON DELETE CASCADE)")




