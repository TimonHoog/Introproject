CREATE table House ( 
houseid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
adress VARCHAR(255),
postcode VARCHAR(255),
huisnummer VARCHAR(255),
oppervlak FLOAT,
beschrijving VARCHAR(1000),
Price DECIMAL (10,2)
);

CREATE table Users (
id INTEGER PRIMARY KEY autoincrement NOT NULL,
homeowner BOOLEAN NOT NULL,
house INTEGER,

FOREIGN KEY (house) REFERENCES "House" (houseid)
	ON UPDATE cascade ON DELETE cascade
);

/* Deze gebruikt voor het maken van de users tabel in de db
("CREATE TABLE Users (id INTEGER PRIMARY KEY NOT NULL, name VARCHAR(255), lastname VARCHAR(255), email VARCHAR(255), phone INTEGER, homeowner BOOLEAN, house INTEGER, FOREIGN KEY (house) REFERENCES House (houseid) ON UPDATE CASCADE ON DELETE CASCADE)")
